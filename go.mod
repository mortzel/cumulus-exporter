module gitlab.com/wobcom/cumulus-exporter

go 1.14

require (
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/common v0.10.0
	github.com/vishvananda/netlink v1.1.0
	gitlab.com/wobcom/transceiver-exporter v0.0.0-20200708160704-a5a469bfbea5
	gopkg.in/yaml.v2 v2.3.0
)
